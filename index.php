<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Gayane Kasparova</title>
		<link rel="icon" href="img/favicon.png">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Monda"> 
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<div class="container">
			<div class="lang-pick">
				<span class="lang" id="en">EN </span>
				<span class="lang" id="ru">РУ</span>
				<span class="lang" id="hy">Հայ </span>
			</div>
			<div id="include">
				
			</div>
		</div>
		<footer>
			<div class="container">
				<p> 2016 &#xA9; Created by Gayane Kasparova</p>
			</div>
		</footer>

    	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    	<script type="text/javascript"> 
    		$( document ).ready(function() {
    			$("#include").load('lang/en/index.php');
			
			$(".lang").click(function(){
	        		lang = $(this).attr("id");
	        		$("#include").load('lang/' + lang + '/index.php');
	       		 }); 
	        });
	    </script> 
	</body>
</html>