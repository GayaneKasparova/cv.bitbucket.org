<div class="row contant">			
	<div class="left col-md-3">
		<div class="avatar row">
			<img src="img/avatar.jpg">
		</div>
		<div id="contact" class="info-block">
			<div class="title">Контакты</div>
			<div class="row">
				<div class="col-md-10">
					<a href="https://www.google.com/maps/place/80+Avet+Avetisyan+St,+Yerevan+0012,+%D0%90%D1%80%D0%BC%D0%B5%D0%BD%D0%B8%D1%8F/@40.1988605,44.5026415,18.87z/data=!4m5!3m4!1s0x406abd3e36599a8f:0x7be430eb6c92c3f5!8m2!3d40.1988592!4d44.5030886" target="blank">
						<i class="material-icons">&#xE0C8;</i>ул. В. Амбарцумяна 97 кв. 43, Ереван, Армения
					</a>
				</div>
			</div>
			<div class="row">
				<a href="tel:+37477654227">
					<div class="col-md-10"><i class="material-icons">&#xE0CD;</i> +374 77 654 227	</div>
				</a>	
			</div>
			<div class="row">
				<div class="col-md-10">
				 	<a href="mailto:gayane.kasparova@outlook.com" ><i class="material-icons">&#xE0E1;</i> gayane.kasparova @ outlook.com</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10">
				 	<a href="https://www.linkedin.com/in/gayanekasparova" target="blank">
						<span id="linkedin">
							<img src="img/linkedin-logo.png" alt="linkedin logo">
						</span>
						Gayane Kasparova
					</a>
				</div>
			</div>					
			<div class="row">
				<div class="col-md-10">
					<a href="skype:gayane_kasparova?chat">
						<span id="skype">
							<img src="img/skype-logo.png" alt="skype logo">
						</span>
						gayane_kasparova
					</a>
				</div>
			</div>
		</div>	
		<div  id="languages"  class="info-block">
			<div class="title">Языки</div>
			<div class="col-md-4">Английский</div>
			<div class="progress">
				<div class="progress-bar" style="width: 80%"></div>
			</div>
			<div class="col-md-4">Армянский</div>
			<div class="progress">
				<div class="progress-bar" style="width: 90%"></div>
			</div>
			<div class="col-md-4">Русский</div>
			<div class="progress">
				<div class="progress-bar"></div>
			</div>
		</div>
		<div id="skills" class="info-block">
			<div class="title">Навыки</div>					
			<div class="ul-label">Свободное владение</div>
			<div class="ul">
				<ul>
					<li>HTML</li>
					<li>CSS/Bootstrap</li>
					<li>JavaScript/JQuery</li>
				</ul>
			</div>
			<div class="ul-label">Базовое владение</div>
			<div class="ul">
				<ul>
					<li>PHP/Laravel</li>
					<li>MySQL</li>
					<li>C#</li>
				</ul>
			</div>
			<div class="ul-label">Понимание ООП</div>
		</div>
		<div id="tools" class="info-block">
			<div class="title">Инструменты</div>
			<div class="ul-label">Свободное владение</div>
			<div class="ul">
				<ul>
					<li>Sublime</li>
					<li>PHPStorm</li>
					<li>Navicat for MySQL</li>
					<li>Wamp/Xampp server</li>
				</ul>
			</div>
			<div class="ul-label">Базовое владение</div>
			<div class="ul">
				<ul>
					<li>TortoiseGit</li>
					<li>Photoshop</li>
					<li>Google Web Designer</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="central col-md-8">
		<h1 class="h1">Гаяне Каспарова</h1>
		<hr>
		<div class="info about"> 	
			<h2 class="h2"><i class="material-icons">&#xE7FD; </i> Обо мне</h2> 
			<p>
				Я начинающий веб-программист. Выпускница политехнического университета.
				Будучи студенткой начала подрабатывать в кол-центре, имела карьерный рост.
				В конце прошлого года ушла из компании и занялась самообразованием. <br>
				Работала волонтером в маленькой частной фирме. <br>
				Ныне я готова занять позицию младшего веб-программиста и стать частью профессиональной команды. 
			</p>
		</div>

		<div class="info work"> 
			<h2 class="h2"><i class="material-icons">&#xE8F9; </i> Опыт работы</h2> 
			<p>
				<span class="title">Июн 2016 - Сен 2016 <span> I Imagine.org </span></span>
				<p> 
					Участие в обработке и создании социальной сети ( <a href="https://iimagine.org/" target="blank">www.iimagine.org</a> )
				</p>
			</p>
			<p>
				<span class="title">Июнь 2016 — Настоящее время <span> "Alliance Construction" LLC</span></span>
				<p> 
					Создание веб сайта компании ( <a href="http://allianceconstruction.am/" target="blank">www.allianceconstruction.am</a> )
				</p>
			</p>
			<p>
				<span class="title">Янв 2012 — Ноя 2015 <span> "Interactive TV" LLC</span></span>
				<ul>
					<li>Оператор кол центра - 12 месяцев</li>
					<li>Специалист технической поддержки - 17 месяцев</li>
					<li>Менеджееер кол центра - 16 месяцев</li>
				</ul>
			</p>
		</div>
		<div class="info education">
			<h2 class="h2"><i class="material-icons">&#xE80C; </i> Образование</h2> 
			<span class="title"> Oct 2014 - Dec 2016 <span> Национальный Политехнический Университет Армении </span></span>
			<p> 
				Менеджмент ИТ организаций ( Магистратура )
			</p>
			<span class="title"> Сен 2009 - Янв 2014 <span> Национальный Политехнический Университет Армении </span></span>
			<p> 
					ИТ в бановском секторе ( Бакалавр )
			</p>
		</div>
		<div class="info courses">
			<h2 class="h2"><i class="material-icons">&#xE86E; </i> Курсы и Трейнинги </h2> 
			<p>
				<ul>
					<li> Курс веб програмирования в трейнинг центре AGH ( HTML, CSS/Bootstrap JavaScript / jQuery & PHP / MySQL & Wordpress )</li>
					<li>C# .NET програмирование в центре образования Microsoft ( C#, Windows Forms, ADO.NET )</li>
				</ul>
			</p>	
		</div>
	</div>
</div>