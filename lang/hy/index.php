<?php $title = "Գայանե Կասպարովա"?>
<div class="row contant">			
	<div class="left col-md-3">
		<div class="avatar row">
			<img src="img/avatar.jpg">
		</div>
		<div id="contact" class="info-block">
			<div class="title">Կոնտակտներ</div>
			<div class="row">
				<div class="col-md-10">
				<a href="https://www.google.com/maps/place/80+Avet+Avetisyan+St,+Yerevan+0012,+%D0%90%D1%80%D0%BC%D0%B5%D0%BD%D0%B8%D1%8F/@40.1988605,44.5026415,18.87z/data=!4m5!3m4!1s0x406abd3e36599a8f:0x7be430eb6c92c3f5!8m2!3d40.1988592!4d44.5030886" target="blank">
					<i class="material-icons">&#xE0C8;</i>ՀՀ, ք. Երևան, Վ․Համբարձումյան 97 բն․ 43 
					</a>
				</div>
			</div>
			<div class="row">
				<a href="tel:+37477654227">
					<div class="col-md-10"><i class="material-icons">&#xE0CD;</i> +374 77 654 227	</div>
				</a>
			</div>
			<div class="row">
				<div class="col-md-10">
				 	<a href="mailto:gayane.kasparova@outlook.com" ><i class="material-icons">&#xE0E1;</i> gayane.kasparova @ outlook.com</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10">
				 	<a href="https://www.linkedin.com/in/gayanekasparova" target="blank">
						<span id="linkedin">
							<img src="img/linkedin-logo.png" alt="linkedin logo">
						</span>
						Gayane Kasparova
					</a>
				</div>
			</div>					
			<div class="row">
				<div class="col-md-10">
					<a href="skype:gayane_kasparova?chat">
						<span id="skype">
							<img src="img/skype-logo.png" alt="skype logo">
						</span>
						gayane_kasparova
					</a>
				</div>
			</div>
		</div>	
		<div  id="languages"  class="info-block">
			<div class="title">Լեզուներ</div>
			<div class="col-md-4">Անգլերեն</div>
			<div class="progress">
				<div class="progress-bar" style="width: 80%"></div>
			</div>
			<div class="col-md-4">Հայերեն</div>
			<div class="progress">
				<div class="progress-bar" style="width: 90%"></div>
			</div>
			<div class="col-md-4">Ռուսերեն</div>
			<div class="progress">
				<div class="progress-bar"></div>
			</div>
		</div>
		<div id="skills" class="info-block">
			<div class="title">Հմտություններ</div>					
			<div class="ul-label">Վարժ տիրապետում</div>
			<div class="ul">
				<ul>
					<li>HTML</li>
					<li>CSS/Bootstrap</li>
					<li>JavaScript/JQuery</li>
				</ul>
			</div>
			<div class="ul-label">Միջին տիրապետում</div>
			<div class="ul">
				<ul>
					<li>PHP/Laravel</li>
					<li>MySQL</li>
					<li>C#</li>
				</ul>
			</div>
			<div class="ul-label">ՕԿԾ գաղափարի ըմբռնում </div>
		</div>
		<div id="tools" class="info-block">
			<div class="title">Գործիքներ</div>
			<div class="ul-label">Վարժ տիրապետում</div>
			<div class="ul">
				<ul>
					<li>Sublime</li>
					<li>PHPStorm</li>
					<li>Navicat for MySQL</li>
					<li>Wamp/Xampp server</li>
				</ul>
			</div>
			<div class="ul-label">Միջին տիրապետում</div>
			<div class="ul">
				<ul>
					<li>TortoiseGit</li>
					<li>Photoshop</li>
					<li>Google Web Designer</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="central col-md-8">
		<h1 class="h1">Գայանե Կասպարովա</h1>
		<hr>
		<div class="info about"> 	
			<h2 class="h2"><i class="material-icons">&#xE7FD; </i> Իմ մասին</h2> 
			<p>
				Ես սկսնակ վեբ ծրագրավորող եմ: Ավարտել եմ ՀՊՃՀ-ն։ ՈՒսանողական տարիներին աշխատել եմ զանգերի կենտրոնում, ունեցել եմ կարիերային աճ: 2015-ի նոյեմբերին դուրս էմ եկել աշխատանքից և զբաղվել ինքնակրթությամբ: <br>
				Աշխատել եմ որպես կամավոր փոքր մասնավոր ընկերությունում։ <br>
				Այժմ պատրաստ եմ աշխատել որպես կրտսեր վեբ ծրագրավորող և դառնալ պրոֆեսիոնալ թիմի մի մաս։
			</p>
		</div>

		<div class="info work"> 
			<h2 class="h2"><i class="material-icons">&#xE8F9; </i> Աշխատանքային փորձ</h2> 
			<p>
				<span class="title">Հուն 2016 - Սեպ 2016 <span>I Imagine.org </span></span>
				<p> 
					Մասնակցություն եմ ունեցել սոցիալական ցանցի կայքի ստեղծման մեջ (<a href="https://iimagine.org/" target="blank">www.iimagine.org</a>)
				</p>
			</p>
			<p>
				<span class="title">Հուն 2016 — Առ այժմ <span>"Ալյանս Քոնսթրաքշն" ՍՊԸ</span></span>
				<p> 
					Կայքի ստեղծում (<a href="http://allianceconstruction.am/" target="blank">www.allianceconstruction.am</a>)
				</p>
			</p>
			<p>
				<span class="title">Հուն 2012 — Նոյ 2015 <span>"Ինտերակտիվ ԹիՎի" ՍՊԸ</span></span>
				<ul>
					<li>Զանգերի Կենտրոնի Օպերատոր - 1 տարի </li>
					<li>Տեխնիկական Աջակցության Մասնագետ - 1.6 տարի</li>
					<li>Զանգերի Կենտրոնի Մենեջեր - 1.4 տարի</li>
				</ul>
			</p>
		</div>
		<div class="info education">
			<h2 class="h2"><i class="material-icons">&#xE80C; </i> Կրթություն</h2> 
			<span class="title"> Հոկ 2014 - Դեկ 2016 <span> Հայաստաբի Ազգային Պոլիտեխնիկական Համալսարանում</span></span>
			<p> 
				ՏՏ ոլորտի ձեռնարկությունների մենեջմենթ (Մագիստրատուրա)
			</p>
			<span class="title"> Սեպ 2009 - Հուն 2014 <span> Հայաստաբի Ազգային Պոլիտեխնիկական Համալսարանում</span></span>
			<p> 
					ՏՏ կիրառումը բանկային համակարգում (Բակալավր)
			</p>
		</div>
		<div class="info courses">
			<h2 class="h2"><i class="material-icons">&#xE86E; </i> Դասընթացներ և Վերապատրաստում </h2> 
			<p>
				<ul>
					<li>Վեբ ծրագրավորման դասընթաց AGH ուսումնական կենտրոնում (HTML, CSS/Bootstrap JavaScript / jQuery & PHP / MySQL & Wordpress)</li>
					<li>C# - .NET ծրագրավորման դասընթաց Microsoft ուսումնական կենտրոնում (C#, Windows Forms, ADO.NET)</li>
				</ul>
			</p>	
		</div>
	</div>
</div>
