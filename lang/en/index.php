<div class="row contant">			
	<div class="left col-md-3">
		<div class="avatar row">
			<img src="img/avatar.jpg">
		</div>
		<div id="contact" class="info-block">
			<div class="title">Contact</div>
			<div class="row">
				<div class="col-md-10">
					<a href="https://www.google.com/maps/place/80+Avet+Avetisyan+St,+Yerevan+0012,+%D0%90%D1%80%D0%BC%D0%B5%D0%BD%D0%B8%D1%8F/@40.1988605,44.5026415,18.87z/data=!4m5!3m4!1s0x406abd3e36599a8f:0x7be430eb6c92c3f5!8m2!3d40.1988592!4d44.5030886" target="blank">
						<i class="material-icons">&#xE0C8;</i>
						V. Hambardzumyan 97, ap 43, Yerevan, Armenia
					</a>
				</div>
			</div>
			<div class="row">
				<a href="tel:+37477654227">
					<div class="col-md-10"><i class="material-icons">&#xE0CD;</i> +374 77 654 227	</div>
				</a>
			</div>
			<div class="row">
				<div class="col-md-10">
				 	<a href="mailto:gayane.kasparova@outlook.com" ><i class="material-icons">&#xE0E1;</i> gayane.kasparova @ outlook.com</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10">
				 	<a href="https://www.linkedin.com/in/gayanekasparova" target="blank">
						<span id="linkedin">
							<img src="img/linkedin-logo.png" alt="linkedin logo">
						</span>
						Gayane Kasparova
					</a>
				</div>
			</div>					
			<div class="row">
				<div class="col-md-10">
					<a href="skype:gayane_kasparova?chat">
						<span id="skype">
							<img src="img/skype-logo.png" alt="skype logo">
						</span> 
						gayane_kasparova
					</a>
				</div>
			</div>
		</div>	
		<div  id="languages"  class="info-block">
			<div class="title">Languages</div>
			<div class="col-md-4">English</div>
			<div class="progress">
				<div class="progress-bar" style="width: 80%"></div>
			</div>
			<div class="col-md-4">Armenian</div>
			<div class="progress">
				<div class="progress-bar" style="width: 90%"></div>
			</div>
			<div class="col-md-4">Russian</div>
			<div class="progress">
				<div class="progress-bar"></div>
			</div>
		</div>
		<div id="skills" class="info-block">
			<div class="title">Skills</div>					
			<div class="ul-label">Strong Knowledge</div>
			<div class="ul">
				<ul>
					<li>HTML</li>
					<li>CSS/Bootstrap</li>
					<li>JavaScript/JQuery</li>
				</ul>
			</div>
			<div class="ul-label">Basic Knowledge</div>
			<div class="ul">
				<ul>
					<li>PHP/Laravel</li>
					<li>MySQL</li>
					<li>C#</li>
				</ul>
			</div>
			<div class="ul-label">Understanding of OOP</div>
		</div>
		<div id="tools" class="info-block">
			<div class="title">Tools</div>
			<div class="ul-label">Strong Knowledge</div>
			<div class="ul">
				<ul>
					<li>Sublime</li>
					<li>PHPStorm</li>
					<li>Navicat for MySQL</li>
					<li>Wamp/Xampp server</li>
				</ul>
			</div>
			<div class="ul-label">Basic Knowledge</div>
			<div class="ul">
				<ul>
					<li>TortoiseGit</li>
					<li>Photoshop</li>
					<li>Google Web Designer</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="central col-md-8">
		<h1 class="h1">Gayane Kasparova</h1>
		<hr>
		<div class="info about"> 	
			<h2 class="h2"><i class="material-icons">&#xE7FD; </i> About me</h2> 
			<p>
			I am an entry level web developer. <br> When I was a student I started working in a call center as a side job and had a career growth. <br>
			At the end of the last year, I left that job and continued education in my main profession. <br>
			Then I have worked as a volunteer in a small private company. <br>
			Now, I'm ready to take up the role of "Junior Web Developer" and be part of a professional team. 
			</p>
		</div>
		<div class="info work"> 
			<h2 class="h2"><i class="material-icons">&#xE8F9; </i> Work Experience</h2> 
			<p>
				<span class="title">Jun 2016 - Sep 2016 <span> I Imagine.org </span></span>
				<p> 
					Participation in social network website development (<a href="https://iimagine.org/" target="blank">www.iimagine.org</a> )
				</p>
			</p>
			<p>
				<span class="title">Jun 2016 — Present <span> "Alliance Construction" LLC</span></span>
				<p> 
					Building company website (<a href="http://allianceconstruction.am/" target="blank">www.allianceconstruction.am</a> )
				</p>
			</p>
			<p>
				<span class="title">Jan 2012 — Nov 2015 <span> "Interactive TV" LLC</span></span>
				<ul>
					<li>Call Center Operator - 1 year </li>
					<li>Technical Support Specialist - 1.6 years</li>
					<li>Manager of Call Center - 1.4 years</li>
				</ul>
			</p>
		</div>
		<div class="info education">
			<h2 class="h2"><i class="material-icons">&#xE80C; </i> Education</h2> 
			<span class="title"> Oct 2014 - Dec 2016 
				<span>State Engineering University of Armenia</span>
			</span>
			<p> 
				Management of IT organizations (Master's Degree )
			</p>
			<span class="title"> Sep 2009 - Jun 2014 
				<span> State Engineering University of Armenia</span>
			</span>
			<p> 
				IT of banking sector (Bachelor's Degree )
			</p>
		</div>
		<div class="info courses">
			<h2 class="h2"><i class="material-icons">&#xE86E; </i> Courses and Trainings </h2> 
			<p>
				<ul>
					<li>WEB Development Course at AGH Training Center (HTML, CSS/Bootstrap JavaScript / jQuery & PHP / MySQL & Wordpress )</li>
					<li>C# - .NET Programming at Education Center of Microsoft (C#, Windows Forms, ADO.NET )</li>
				</ul>
			</p>	
		</div>
	</div>				
</div>
